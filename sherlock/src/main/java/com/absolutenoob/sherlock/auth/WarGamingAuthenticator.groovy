package com.absolutenoob.sherlock.auth

import com.absolutenoob.sherlock.core.wargaming.Tanker
import com.google.common.base.Optional
import io.dropwizard.auth.AuthenticationException
import io.dropwizard.auth.Authenticator

/**
 * Created by quincy on 15-04-21.
 */
class WarGamingAuthenticator implements Authenticator<WarGamingCredentials, Tanker> {
    @Override
    Optional<Tanker> authenticate(WarGamingCredentials warGamingCredentials) throws AuthenticationException {
        String sessionID = warGamingCredentials.getSessionID()
        def storedCredentials = CredentialsCache.instance().get(sessionID)
        if (storedCredentials.isPresent()) {
            //check for valid role
            return Optional.fromNullable(storedCredentials.get().getTanker())
        }
        return Optional.absent()
    }
}

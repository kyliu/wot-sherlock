package com.absolutenoob.sherlock.auth

import com.absolutenoob.sherlock.core.wargaming.Tanker

/**
 * Created by quincy on 15-04-21.
 */
class WarGamingCredentials {
    String sessionID
    Tanker tanker

    public WarGamingCredentials(String sessionID, Tanker tanker) {
        this.sessionID = sessionID
        this.tanker = tanker
    }

    static WarGamingCredentials createTmpCredential(String sessionID) {
        return new WarGamingCredentials(sessionID, null)
    }

    public boolean equals(Object object) {
        if (object != null && object instanceof WarGamingCredentials) {
            WarGamingCredentials otherCredentials = (WarGamingCredentials) object
            if (this.getSessionID().equals(otherCredentials.getSessionID()) &&
                    this.tanker.getAccessToken().equals(otherCredentials.getTanker().getAccessToken()) &&
                    this.tanker.getAccountID() == otherCredentials.getTanker().getAccessToken()) {
                return true
            }
        }
        return false;
    }

    public void merge(WarGamingCredentials otherCredentials) {
        if (this.sessionID.equals(otherCredentials.getSessionID())) {
            this.tanker = otherCredentials.getTanker()
        }
    }
}

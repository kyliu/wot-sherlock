package com.absolutenoob.sherlock.auth

/**
 * Created by quincy on 15-04-22.
 */
class CredentialsCache<String, WarGamingCredentials>{
    public static CredentialsCache<String, WarGamingCredentials> INSTANCE

    Map<String, WarGamingCredentials> cache

    private CredentialsCache() {
        cache = new HashMap<String, WarGamingCredentials>()
    }

    static CredentialsCache<String, WarGamingCredentials> instance() {
        if (INSTANCE == null) {
            INSTANCE = new CredentialsCache<String, WarGamingCredentials>()
        }
        return INSTANCE
    }

    public void put(String sessionID, WarGamingCredentials credentials) {
        def t = cache.get(sessionID)
        if (t) {
            t.merge(credentials)
        }
        cache.put(sessionID, credentials)
    }

    public Optional<WarGamingCredentials> get(String sessionID) {
        def result = cache.get(sessionID)
        if (result == null) {
            return Optional.empty()
        }
        return Optional.of(result)
    }
}

package com.absolutenoob.sherlock.auth

import io.dropwizard.auth.Authenticator
import io.dropwizard.auth.AuthFactory
import io.dropwizard.auth.AuthenticationException

import javax.servlet.http.HttpServletRequest
//import javax.ws.rs.WebApplicationException
import javax.ws.rs.core.Context

/**
 * Created by quincy on 15-04-28.
 */
public class SessionUserAuthFactory<T> extends AuthFactory<WarGamingCredentials, T> {
    public static final String SESSION_ID = "session_id"

    @Context
    private HttpServletRequest request
    private final boolean required
//    private UnauthorizedHandler unauthorizedHandler = new HogeUnauthorizedHandler();
    private final Class<T> generatedClass
    private String prefix = SESSION_ID
    private final String realm

    public SessionUserAuthFactory(Authenticator<WarGamingCredentials, T> authenticator, final String realm,
                                  final Class<T> generatedClass) {
        super(authenticator)
        this.required = false
        this.realm = realm
        this.generatedClass = generatedClass
    }

    private SessionUserAuthFactory(final boolean required, final Authenticator<WarGamingCredentials, T> authenticator,
                                   final String realm, final Class<T> generatedClass) {
        super(authenticator)
        this.required = required
        this.realm = realm
        this.generatedClass = generatedClass
    }
//
//    public SessionUserAuthFactory<T> responseBuilder(UnauthorizedHandler unauthorizedHandler) {
//        this.unauthorizedHandler = unauthorizedHandler;
//        return this;
//    }

    @Override
    public T provide() {
        Optional<WarGamingCredentials> credentials = Optional.ofNullable(request)
                .map{req -> req.getCookies()}
                .map{cookies -> Arrays.asList(cookies).stream()
                .filter{cookie -> SESSION_ID.equals(cookie.getName())}
                .findFirst().orElseGet(null)}
                .map{sidcookie -> sidcookie.getValue()}
                .map{sid -> WarGamingCredentials.createTmpCredential(sid)}

        if (credentials.isPresent()) {
            try {
                def result = authenticator().authenticate(credentials.get())
                return result.orNull()
            } catch (AuthenticationException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        if (required) {
//            throw new WebApplicationException(unauthorizedHandler.buildResponse(prefix, realm));
            throw new AuthenticationException()
        }

        return null;
    }

    @Override
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    @Override
    public AuthFactory<WarGamingCredentials, T> clone(boolean required) {
        return new SessionUserAuthFactory<>(required, authenticator(), this.realm, this.generatedClass)
    }

    @Override
    public Class<T> getGeneratedClass() {
        return generatedClass;
    }

}

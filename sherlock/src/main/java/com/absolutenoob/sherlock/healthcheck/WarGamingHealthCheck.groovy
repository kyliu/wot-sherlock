package com.absolutenoob.sherlock.healthcheck

import com.absolutenoob.sherlock.configuration.WarGamingConfiguration
import com.codahale.metrics.health.HealthCheck
import static com.codahale.metrics.health.HealthCheck.Result

import javax.ws.rs.client.Client
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

/**
 * Created by quincy on 15-04-19.
 */
class WarGamingHealthCheck extends HealthCheck {
    Client client
    WarGamingConfiguration configuration

    public WarGamingHealthCheck(Client client, WarGamingConfiguration configuration) {
        this.client = client
        this.configuration = configuration
    }

    @Override
    protected Result check() throws Exception {
        def target = client.target(configuration.wotAPI)
        def response = target.request(MediaType.APPLICATION_JSON_TYPE).get()

        if (Response.Status.OK.getStatusCode() == response.getStatus()) {
            return Result.healthy("War Gaming API is reachable.")
        } else {
            return Result.unhealthy("Unable to reach War Gaming API")
        }
    }
}

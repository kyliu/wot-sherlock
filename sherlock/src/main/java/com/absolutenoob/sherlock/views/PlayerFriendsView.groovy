package com.absolutenoob.sherlock.views

import com.absolutenoob.sherlock.core.wargaming.PlayerInfo
import io.dropwizard.views.View

import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.PathParam
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

/**
 * Created by quincy on 15-05-21.
 */
@Path("/player/")
@Produces(MediaType.TEXT_HTML)
class PlayerFriendsView extends View {
    Collection<PlayerInfo> playerInfos

    protected PlayerFriendsView(Collection<PlayerInfo> playerInfos) {
        super("playerfriends.ftl")
        this.playerInfos = playerInfos
    }

    public Collection<PlayerInfo> getPlayerInfos() {
        return this.playerInfos
    }
}

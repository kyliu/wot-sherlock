package com.absolutenoob.sherlock.core.wargaming

import com.fasterxml.jackson.annotation.JsonProperty

import java.sql.Timestamp

/**
 * Created by quincy on 15-05-21.
 */
class PlayerInfo {
    @JsonProperty("nickname")
    String nickname
    @JsonProperty("logout_at")
    long logoutTime
    @JsonProperty("last_battle_time")
    long lastBattleTime
    @JsonProperty("created_at")
    long createdTime

    long accountID
    Object data
    Utils utils = new Utils()

    public getLogOut() {
        return utils.createTimestamp(logoutTime)
    }

    public getLastBattle() {
        return utils.createTimestamp(this.lastBattleTime)
    }

    public getCreated() {
        return utils.createTimestamp(this.createdTime)
    }
}

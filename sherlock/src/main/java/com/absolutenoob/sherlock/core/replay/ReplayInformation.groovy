package com.absolutenoob.sherlock.core.replay

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Created by quincy on 15-04-20.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class ReplayInformation {
    @JsonProperty("common")
    Common common
    @JsonProperty("datablock_1")
    Datablock1 datablock1
    @JsonProperty("datablock_battle_result")
    DatablockBattleResult datablockBattleResult
    @JsonProperty("identify")
    Identify identify

    @JsonIgnoreProperties(ignoreUnknown = true)
    class Common {
        @JsonProperty("datablock_1")
        int datablock1
        @JsonProperty("datablock_battle_result")
        int datablockBattleResult
        @JsonProperty("message")
        String message
        @JsonProperty("parser")
        String parser
        @JsonProperty("replay_version")
        String replayVersion
        @JsonProperty("status")
        String status
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    class Datablock1 {
        @JsonProperty("battleType")
        int battleType
        @JsonProperty("clientVersionFromExe")
        String clientExeVersion
        @JsonProperty("clientVersionFromXml")
        String clientXmlVersion
        @JsonProperty("dateTime")
        String dateTime
        @JsonProperty("mapDisplayName")
        String mapDisplayName
        @JsonProperty("mapName")
        String mapName
        @JsonProperty("playerID")
        long playerID
        @JsonProperty("playerName")
        String playerName
        @JsonProperty("playerVehicle")
        String playerVehicle
        @JsonProperty("regionCode")
        String regionCode
        @JsonProperty("vehicles")
        Map<Long, Vehicle> vehicles
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    class DatablockBattleResult {
        @JsonProperty("arenaUniqueID")
        long arenaUniqueID
        @JsonProperty("common")
        Common common
        @JsonProperty("players")
        Map<Long, Player> players
        @JsonProperty("vehicles")
        Map<Long, VehiclePlayerInfo> vehicles

        @JsonIgnoreProperties(ignoreUnknown = true)
        class Common {
            @JsonProperty("arenaCreateTime")
            long arenaCreateTime
            @JsonProperty("arenaTypeID")
            int arenaTypeID
            @JsonProperty("bonusType")
            int bonusType
            @JsonProperty("duration")
            long duration
            @JsonProperty("finishReason")
            int finishReason
            @JsonProperty("guiType")
            int guiType
            @JsonProperty("vehLockMode")
            int vehLockMode
            @JsonProperty("winnerTeam")
            int winnerTeam
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    class Identify {
        @JsonProperty("accountDBID")
        String accountDBID
        @JsonProperty("arenaCreateTime")
        long arenaCreateTime
        @JsonProperty("arenaUniqueID")
        long arenaUniqueID
        @JsonProperty("internaluserID")
        long internalUserID
        @JsonProperty("mapName")
        String mapName
        @JsonProperty("mapid")
        int mapID
        @JsonProperty("playerName")
        String playerName
        @JsonProperty("replay_version")
        String replayVersion
    }

    void updateInformation() {
        this.datablock1.getVehicles().each { key, value -> value.setVehicleBattleId(key) }
        this.datablockBattleResult.getVehicles().each { key, value -> value.setVehicleBattleId(key) }
        this.datablockBattleResult.getPlayers().each { key, value -> value.setAccountId(key) }
    }

    List<PlayerBattleInformation> getPlayBattleinformation() {
        def playerBattleInfos = new LinkedList<PlayerBattleInformation>()
        this.datablockBattleResult.getVehicles().each { key, value ->
            def player = this.datablockBattleResult.getPlayers().get(value.getAccountID())
            def vehicle = this.datablock1.getVehicles().get(key)
            def info = PlayerBattleInformation.createPlayerBattleInformation(value, player, vehicle)
            playerBattleInfos.add(info)
        }
        return playerBattleInfos
    }
}



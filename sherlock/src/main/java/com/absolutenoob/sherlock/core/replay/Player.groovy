package com.absolutenoob.sherlock.core.replay

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Created by quincy on 15-04-13.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class Player {
    long accountId
    @JsonProperty("clanAbbrev")
    String clanAbbrev
    @JsonProperty("clanDBID")
    long clanID
    @JsonProperty("name")
    String name
    @JsonProperty("team")
    int team

    String toString() {
        return "${team} ${accountId} ${name} ${clanAbbrev} ${clanID}"
    }
}

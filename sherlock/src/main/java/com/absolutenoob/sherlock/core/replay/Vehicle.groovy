package com.absolutenoob.sherlock.core.replay

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Created by quincy on 15-04-13.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class Vehicle {
    long vehicleBattleId
    @JsonProperty("team")
    int team
    @JsonProperty("isAlive")
    boolean isAlive
    @JsonProperty("name")
    String name
    @JsonProperty("clanAbbrev")
    String clanAbbrev
    @JsonProperty("vehicleType")
    String vehicleType

    String toString() {
        return "${team} ${vehicleBattleId} ${name} ${clanAbbrev} ${vehicleType} ${isAlive}"
    }

    String getCountry() {
        if (vehicleType == null || vehicleType.indexOf(":") == -1)
            return ""

        return vehicleType.split(":").getAt(0)
    }

    String getVehicleTag() {
        if (vehicleType == null || vehicleType.indexOf(":") == -1)
            return ""

        return vehicleType.split(":").getAt(1)
    }
}

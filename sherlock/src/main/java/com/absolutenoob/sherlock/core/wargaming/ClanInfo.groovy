package com.absolutenoob.sherlock.core.wargaming

import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Created by quincy on 15-04-17.
 */
class ClanInfo {
    @JsonProperty("clan_id")
    long id
    @JsonProperty("name")
    String name
    @JsonProperty("members_count")
    int count
    @JsonProperty("created_at")
    long createdDate
    @JsonProperty("updated_at")
    long updateDate
    @JsonProperty("abbreviation")
    String abbrev
    @JsonProperty("emblems")
    Map<String, String> emblems
    @JsonProperty("members")
    Map<Long, ClanMember> members

    String toString() {
        return "${id} ${name} ${abbrev} ${count} ${createdDate} ${updateDate}"
    }
}

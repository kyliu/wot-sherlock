package com.absolutenoob.sherlock.core.wargaming

import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Created by quincy on 15-04-17.
 */
class ClanSummary {

    @JsonProperty("name")
    String name
    @JsonProperty("abbreviation")
    String abbrev
    @JsonProperty("members_count")
    int count
    @JsonProperty("clan_id")
    long clanID
    @JsonProperty("motto")
    String motto
    @JsonProperty("created_at")
    long createdDate

    String toString() {
        return "${clanID} ${name} ${abbrev} ${count} ${motto} ${createdDate}"
    }
}

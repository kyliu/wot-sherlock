package com.absolutenoob.sherlock.core.wargaming

import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Created by quincy on 15-04-18.
 */
class TankSummary {
    @JsonProperty("tank_id")
    long tankID
    @JsonProperty("short_name")
    String shortName
    @JsonProperty("name")
    String name
    @JsonProperty("tag")
    String tag
    @JsonProperty("nation")
    String nation
    @JsonProperty("type")
    String type
    @JsonProperty("tier")
    int tier

    String toString() {
        return "${tankID} ${name} ${nation} ${type} ${tier}"
    }
}

package com.absolutenoob.sherlock.core.replay

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Created by quincy on 15-04-13.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class VehiclePlayerInfo {
    long vehicleBattleId
    @JsonProperty("accountDBID")
    long accountID
    @JsonProperty("tankID")
    long tankID
    @JsonProperty("team")
    int team

    String toString() {
        return "${team} ${vehicleBattleId} ${accountID} ${tankID}"
    }
}

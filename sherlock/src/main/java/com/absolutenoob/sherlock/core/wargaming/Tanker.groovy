package com.absolutenoob.sherlock.core.wargaming

import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Created by quincy on 15-04-21.
 */
class Tanker {
    @JsonProperty("access_token")
    String accessToken
    @JsonProperty("expires_at")
    long expiresAt
    @JsonProperty("account_id")
    long accountID
    @JsonProperty("nickname")
    String nickname

    String sessionID

    static Tanker createTanker(String accessToken, long accountID, String nickname, long expireAt, String sessionID) {
        def tanker = new Tanker()
        tanker.accessToken = accessToken
        tanker.accountID = accountID
        tanker.nickname = nickname
        tanker.expiresAt = expireAt
        tanker.sessionID = sessionID
        return tanker
    }
}

package com.absolutenoob.sherlock.core.replay
/**
 * Created by quincy on 15-04-13.
 */
class PlayerBattleInformation {
    int team
    String name
    long accountId

    Optional<String> clanAbbrev
    Optional<Long> clanId
    Optional<Long> tankId
    Optional<String> vehicleType

    long vehicleBattleId
    boolean isAlive

    static PlayerBattleInformation createPlayerBattleInformation(VehiclePlayerInfo vpi, Player player, Vehicle vehicle) {
        def result = new PlayerBattleInformation()

        result.setTeam(player.getTeam())
        result.setAccountId(player.getAccountId())
        result.setClanAbbrev(player.getClanAbbrev())
        result.setClanId(player.getClanID())

        if (result.getTeam() != vpi.getTeam()) {
            println "accountID: ${result.getAccountId()} result: ${result.getTeam()} vpi: ${vpi.getTeam()}"
        }

        if (result.getAccountId() != vpi.getAccountID()) {
            println "accountID: ${result.getAccountId()} vpi: ${vpi.getAccountID()}"
        }
        result.setVehicleBattleId(vpi.getVehicleBattleId())
        result.setTankId(vpi.getTankID())

        if (result.getTeam() != vehicle.getTeam()) {
            println "accountID: ${result.getAccountId()} team: ${result.getTeam()} vehicle: ${vehicle.getTeam()}"
        }
        if (result.getVehicleBattleId() != vehicle.getVehicleBattleId()) {
            println "accountID: ${result.getAccountId()} vehicleBattleId: ${result.getVehicleBattleId()} vehicle: ${vehicle.getVehicleBattleId()}"
        }
        if (!result.getClanAbbrev().orElse("").equals(vehicle.getClanAbbrev())) {
            println "accoundId: ${result.getAccountId()} clanAbbrev: ${result.getClanAbbrev()} vehicle: ${vehicle.getClanAbbrev()}"
        }
        result.setName(vehicle.getName())
        result.setVehicleType(vehicle.getVehicleType())
        result.setIsAlive(vehicle.getIsAlive())

        return result
    }

    String toString() {
        return "${team} ${name} ${accountId} ${clanAbbrev.orElse("")} ${clanId.isPresent() ? clanId.get() : ""} ${vehicleBattleId} ${tankId.isPresent() ? tankId.get() : ""} ${vehicleType.orElse("")} ${isAlive}"
    }

    void setClanAbbrev(String clanAbbrev) {
        if (clanAbbrev == null || clanAbbrev.trim().isEmpty()) {
            this.clanAbbrev = Optional.empty()
        } else {
            this.clanAbbrev = Optional.of(clanAbbrev)
        }
    }

    void setClanId(Long clanId) {
        if (clanId == null || clanId == 0L) {
            this.clanId = Optional.empty()
        } else {
            this.clanId = Optional.of(clanId)
        }
    }

    void setTankId(Long tankId) {
        if (tankId == null || tankId == 0) {
            this.tankId = Optional.empty()
        } else {
            this.tankId = Optional.of(tankId)
        }
    }

    void setVehicleType(String vehicleType) {
        if (vehicleType == null || vehicleType.trim().isEmpty()) {
            this.vehicleType = Optional.empty()
        } else {
            this.vehicleType = Optional.of(vehicleType)
        }
    }
}

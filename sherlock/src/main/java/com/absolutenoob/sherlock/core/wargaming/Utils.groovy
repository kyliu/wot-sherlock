package com.absolutenoob.sherlock.core.wargaming

import java.sql.Timestamp

/**
 * Created by quincy on 15-05-21.
 */
class Utils {
    public static final long MILLI_IN_SEC = 1000

    public Timestamp createTimestamp(long timeInSecond) {
        return new Timestamp(timeInSecond * MILLI_IN_SEC)
    }
}

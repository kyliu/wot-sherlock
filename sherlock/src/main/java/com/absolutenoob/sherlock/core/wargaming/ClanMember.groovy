package com.absolutenoob.sherlock.core.wargaming

import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Created by quincy on 15-04-17.
 */
class ClanMember {
    @JsonProperty("account_name")
    String name
    @JsonProperty("account_id")
    long id
    @JsonProperty("role")
    String role
    @JsonProperty("created_at")
    long createdDate

    String toString() {
        return "${id} ${name} ${role} ${createdDate}"
    }
}

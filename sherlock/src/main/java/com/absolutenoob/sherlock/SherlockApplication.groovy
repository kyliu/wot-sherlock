package com.absolutenoob.sherlock

import com.absolutenoob.sherlock.auth.SessionUserAuthFactory
import com.absolutenoob.sherlock.auth.WarGamingAuthenticator
import com.absolutenoob.sherlock.auth.WarGamingCredentials
import com.absolutenoob.sherlock.core.wargaming.Tanker
import com.absolutenoob.sherlock.healthcheck.WarGamingHealthCheck
import com.absolutenoob.sherlock.resources.ClanResources
import com.absolutenoob.sherlock.resources.LogInResources
import com.absolutenoob.sherlock.resources.PlayerResources
import com.absolutenoob.sherlock.resources.ReplayResources
import com.absolutenoob.sherlock.service.WarGamingService
import io.dropwizard.Application
import io.dropwizard.assets.AssetsBundle
import io.dropwizard.auth.AuthFactory
import io.dropwizard.client.JerseyClientBuilder
import io.dropwizard.forms.MultiPartBundle
import io.dropwizard.jersey.sessions.HttpSessionFactory
import io.dropwizard.setup.Bootstrap
import io.dropwizard.setup.Environment
import io.dropwizard.views.ViewBundle
import org.eclipse.jetty.server.session.SessionHandler

/**
 * Created by quincy on 15-04-17.
 */
class SherlockApplication extends Application<SherlockConfiguration> {

    public static void main(String[] args) {
        new SherlockApplication().run(args)
    }

    @Override
    void initialize(Bootstrap<SherlockConfiguration> bootstrap) {
        bootstrap.addBundle(new MultiPartBundle())
        bootstrap.addBundle(new AssetsBundle("/assets/", "/assets/"))
        bootstrap.addBundle(new ViewBundle<SherlockConfiguration>() {
            @Override
            public Map<String, Map<String, String>> getViewConfiguration(SherlockConfiguration config) {
                return config.getViewRendererConfiguration();
            }
        })
    }

    @Override
    void run(SherlockConfiguration sherlockConfiguration, Environment environment) throws Exception {
        def client = new JerseyClientBuilder(environment).using(sherlockConfiguration.getJerseyClientConfiguration())
                .build(getName())

        def warGamingService = new WarGamingService(client, sherlockConfiguration.getWarGamingConfiguration())

        environment.jersey().register(new ClanResources(warGamingService))
        environment.jersey().register(new PlayerResources(warGamingService))
        environment.jersey().register(new LogInResources(warGamingService))
        environment.jersey().register(new ReplayResources())

        environment.jersey().register(HttpSessionFactory.class)
        environment.servlets().setSessionHandler(new SessionHandler())

        def authenticator = new WarGamingAuthenticator()
        def authFactory = new SessionUserAuthFactory<Tanker>(authenticator, "World of Tanks - Sherlock", Tanker.class)

        environment.jersey().register(AuthFactory.binder(authFactory))

        def warGamingHealthCheck = new WarGamingHealthCheck(client, sherlockConfiguration.getWarGamingConfiguration())
        environment.healthChecks().register("War Gaming API Health Check", warGamingHealthCheck)
    }
}

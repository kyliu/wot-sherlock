package com.absolutenoob.sherlock

import com.absolutenoob.sherlock.configuration.WarGamingConfiguration
import com.fasterxml.jackson.annotation.JsonProperty
import io.dropwizard.Configuration
import io.dropwizard.client.JerseyClientConfiguration

import javax.validation.Valid
import javax.validation.constraints.NotNull

/**
 * Created by quincy on 15-04-17.
 */
class SherlockConfiguration extends Configuration {
    @Valid
    @NotNull
    @JsonProperty("httpClient")
    private JerseyClientConfiguration httpClient = new JerseyClientConfiguration()

    @Valid
    @NotNull
    @JsonProperty("warGamingConfiguration")
    private WarGamingConfiguration warGamingConfiguration = new WarGamingConfiguration()

    @NotNull
    private Map<String, Map<String, String>> viewRendererConfiguration = Collections.emptyMap()

    public JerseyClientConfiguration getJerseyClientConfiguration() {
        return httpClient
    }

    public WarGamingConfiguration getWarGamingConfiguration() {
        return warGamingConfiguration
    }

    public Map<String, Map<String, String>> getViewRendererConfiguration() {
        return viewRendererConfiguration
    }
}

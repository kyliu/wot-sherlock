package com.absolutenoob.sherlock.resources

import com.absolutenoob.sherlock.service.JsonMarshalService
import com.absolutenoob.sherlock.service.WarGamingService

import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.PathParam
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

/**
 * Created by quincy on 15-04-17.
 */
@Path("/{region}/clan/")
@Produces(MediaType.APPLICATION_JSON)
class ClanResources {

    JsonMarshalService jsonMarshalService
    WarGamingService warGamingService

    public ClanResources(WarGamingService warGamingService) {
        this.jsonMarshalService = new JsonMarshalService()
        this.warGamingService = warGamingService
    }

    @GET
    @Path('/search/{clanTagSearchTerm}')
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchClanTags(
            @PathParam("region") String region, @PathParam("clanTagSearchTerm") String clanTagSearchTerm) {
        def result = warGamingService.searchClan(clanTagSearchTerm)

        if (result.size() == 0) {
            return Response.noContent().build()
        }

        return Response.ok(jsonMarshalService.marshal(result)).build()
    }

    @GET
    @Path('/{clanTag}')
    @Produces(MediaType.APPLICATION_JSON)
    public Response getClan(@PathParam("region") String region, @PathParam("clanTag") String clanTag) {
        def result = warGamingService.searchClan(clanTag)
        def clan = result.findAll { it -> clanTag.equalsIgnoreCase(it.getAbbrev()) }

        if (clan.isEmpty()) {
            return Response.noContent().build()
        }

        return getClan(region, clan.get(0).getClanID())
    }

    @GET
    @Path('/id/{clanID}')
    @Produces(MediaType.APPLICATION_JSON)
    public Response getClan(@PathParam("region") String region, @PathParam("clanID") long clanID) {
        def result = warGamingService.getClanInfo(clanID)

        if (result.size() == 0) {
            return Response.noContent().build()
        }

        return Response.ok(jsonMarshalService.marshal(result.collect { key, value -> value })).build()
    }
}

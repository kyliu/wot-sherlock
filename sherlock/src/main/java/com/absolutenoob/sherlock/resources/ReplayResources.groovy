package com.absolutenoob.sherlock.resources

import com.absolutenoob.sherlock.core.replay.ReplayInformation
import org.glassfish.jersey.media.multipart.FormDataContentDisposition
import org.glassfish.jersey.media.multipart.FormDataParam

import javax.validation.Valid
import javax.ws.rs.*
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response
import java.nio.charset.Charset
import java.nio.file.FileSystems
import java.nio.file.Files
import java.nio.file.StandardCopyOption

/**
 * Created by quincy on 15-04-19.
 */
@Path("/{region}/replay")
class ReplayResources {
    String replayDirectory = "./"
    String jsonDirectory = "./"

    @POST
    @Path("/uploadfile")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public Response convertReplay(@PathParam("region") String region, @FormDataParam("filename") String filename,
                                  @FormDataParam("file") final InputStream fileInputStream,
                                  @FormDataParam("file") final FormDataContentDisposition fileDetail) {

        def outputPath = FileSystems.getDefault().getPath(replayDirectory, filename)
        Files.copy(fileInputStream, outputPath, StandardCopyOption.REPLACE_EXISTING)

        def replayFilename = outputPath.toAbsolutePath().toString()

        int rc = executeOnShell("/Users/quincy/Development/wot-sherlock/sherlock/target/classes/WoT-Replay-To-JSON/wotrp2j.py",
                replayFilename)

        if (rc == 0) {
            int fileExtensionIndex = replayFilename.lastIndexOf('.')
            def jsonFilename = replayFilename.subSequence(0, fileExtensionIndex) + ".json"
            def jsonFilePath = FileSystems.getDefault().getPath(jsonFilename)
            String result = Files.readAllLines(jsonFilePath, Charset.defaultCharset()).toString()
            return Response.ok(result).build()
        } else {
            return Response.serverError().build()
        }
    }

    int executeOnShell(String... command) {
        println command
        def process = new ProcessBuilder(command)
                .redirectErrorStream(true)
                .start()

        process.inputStream.eachLine { println it }
        process.waitFor()
        return process.exitValue()
    }

    @POST
    @Path("/uploadresult")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response submitReplayInformation(
            @PathParam("region") String region, @Valid ReplayInformation replayInformation) {

        replayInformation.updateInformation()
        def playerBattleInfos = replayInformation.getPlayBattleinformation()

        return Response.ok(playerBattleInfos).build()
    }
}

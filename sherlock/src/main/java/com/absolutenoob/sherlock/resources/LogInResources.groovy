package com.absolutenoob.sherlock.resources

import com.absolutenoob.sherlock.auth.CredentialsCache
import com.absolutenoob.sherlock.auth.WarGamingCredentials
import com.absolutenoob.sherlock.core.wargaming.Tanker
import com.absolutenoob.sherlock.service.JsonMarshalService
import com.absolutenoob.sherlock.service.WarGamingService
import io.dropwizard.auth.Auth

import javax.servlet.http.HttpServletRequest
import javax.ws.rs.CookieParam
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.Context
import javax.ws.rs.core.Cookie
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.NewCookie
import javax.ws.rs.core.Response
import javax.ws.rs.core.UriInfo

/**
 * Created by quincy on 15-04-21.
 */
@Path("/{region}/login")
@Produces(MediaType.TEXT_HTML)
class LogInResources {
    JsonMarshalService jsonMarshalService
    WarGamingService warGamingService

    public LogInResources(WarGamingService warGamingService) {
        this.warGamingService = warGamingService
        this.jsonMarshalService = new JsonMarshalService()
    }

    @GET
    @Path("/")
    @Produces(MediaType.TEXT_HTML)
    public Response login(@Auth(required = false) Tanker tanker, @Context HttpServletRequest request, @CookieParam("session_id") String sessionID) {
        def session = request.getSession(true)

//        def sessionID = session.getId()
        println "sessionID from http session is ${sessionID}"
//        if (sessionID == null) {
//            sessionID = UUID.randomUUID().toString()
//            session.setAttribute("session_id", sessionID)
//        }

        def cookie = new NewCookie("session_id", sessionID)
        def c = WarGamingCredentials.createTmpCredential(sessionID)
        CredentialsCache.instance().put(c.getSessionID(), c)
        def location = warGamingService.login("http://localhost:8080/na/login/verify/?session_id=${session.getId()}")
        return Response.seeOther(URI.create(location)).cookie(cookie).build()
    }

    @GET
    @Path("/verify")
    @Produces(MediaType.APPLICATION_JSON)
    public Response authenticate(@Auth(required = false) Tanker tanker, @Context UriInfo uriInfo, @Context HttpServletRequest request, @CookieParam("session_id") String sessionID) {
        def session = request.getSession()

        println "sessionID is ${sessionID}"
        if (sessionID == null) {
            sessionID = UUID.randomUUID().toString()
        }

        def queryParameters = uriInfo.getQueryParameters()
        def status = queryParameters.get("status").get(0)
        def result = [:]

        if ("ok".equals(status)) {
            def accessToken = queryParameters.get("access_token").get(0).toString()
            def expiresAt = queryParameters.get("expires_at").get(0).toString().toLong()
            def accountID = queryParameters.get("account_id").get(0).toString().toLong()
            def nickname = queryParameters.get("nickname").get(0).toString()
            def t = Tanker.createTanker(accessToken, accountID, nickname, expiresAt, sessionID)
            CredentialsCache.instance().put(sessionID, new WarGamingCredentials(sessionID, t))
            result.put(status, t)
        }

        if ("error".equals(status)) {
            def code = queryParameters.get(0).get("code")
            def message = queryParameters.get(0).get("message")
            result.put(status, ["code": code, "message" : message])
        }

        def cookie = new NewCookie("session_id", sessionID)
        return Response.ok(result).cookie(cookie).build()
    }
}

package com.absolutenoob.sherlock.resources

import com.absolutenoob.sherlock.core.wargaming.PlayerInfo
import com.absolutenoob.sherlock.core.wargaming.TankSummary
import com.absolutenoob.sherlock.core.wargaming.Tanker
import com.absolutenoob.sherlock.service.JsonMarshalService
import com.absolutenoob.sherlock.service.WarGamingService
import com.absolutenoob.sherlock.views.PlayerFriendsView
import io.dropwizard.auth.Auth

import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.PathParam
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

/**
 * Created by quincy on 15-04-18.
 */

@Path("/{region}/player/")
@Produces(MediaType.APPLICATION_JSON)
class PlayerResources {
    WarGamingService warGamingService
    JsonMarshalService jsonMarshalService

    public PlayerResources(WarGamingService warGamingService) {
        this.warGamingService = warGamingService
        this.jsonMarshalService = new JsonMarshalService()
    }

    @GET
    @Path('{playerTag}/{tier}')
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPlayerTanks(@Auth Tanker tanker,
            @PathParam("region") String region, @PathParam("playerTag") String playerTag, @PathParam("tier") int tier) {
        def tanks = warGamingService.getTankSummaryByTier(tier)

        def player = warGamingService.searchPlayer(playerTag, "exact")
        long accountID = player.get(0).get("account_id")

        def playerTanks = warGamingService.getPlayerVehicles([accountID], tanks.keySet().toList())

        def playerTankIDs = playerTanks.get(accountID).collect { Map it -> Long.valueOf(it.get("tank_id")) }

        def result = tanks.findAll { Long key, TankSummary ts -> playerTankIDs.contains(key) }

        return Response.ok(result).build()
    }

    @GET
    @Path('{playerID}')
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPlayerStat(@Auth Tanker tanker,
                                   @PathParam("region") String region, @PathParam("playerID") long playerID) {
        warGamingService.searchClan()
        def player = warGamingService.searchPlayer(playerTag, "exact")
        long accountID = player.get(0).get("account_id")

        def playerTanks = warGamingService.getPlayerVehicles([accountID], tanks.keySet().toList())

        def playerTankIDs = playerTanks.get(accountID).collect { Map it -> Long.valueOf(it.get("tank_id")) }

        def result = tanks.findAll { Long key, TankSummary ts -> playerTankIDs.contains(key) }

        return Response.ok(result).build()
    }

    @GET
    @Path('{playerTag}/friends')
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPlayerFriends(@Auth Tanker tanker,
                                   @PathParam("region") String region, @PathParam("playerTag") String playerTag) {

        def player = warGamingService.searchPlayer(playerTag, "exact")
        long accountID = player.get(0).get("account_id")

        def friends = warGamingService.getPlayerFriends(accountID, "85f5e4046c243f613a6013a514108ba5068d3120")

        Map<String, PlayerInfo> playerInfo = [:]

        int count = 0
        int maxSize = 100

        while (friends.size() > count * maxSize) {
            int start = count * maxSize
            int end = (count + 1) * maxSize - 1
            end = end > (friends.size() - 1) ? friends.size() - 1 : end;
            def temp = friends[start..end]
            warGamingService.getPlayerInfo(temp).each {Long id, PlayerInfo info ->
                info.setAccountID(id)
                playerInfo.put(info.getNickname(), info)
            }
            count = count + 1
        }

        Map<String, String> players = [:]
        playerInfo.each { String key, PlayerInfo info ->
            players.put(info.getNickname(),
                    "http://www.noobmeter.com/player/na/${info.getNickname()}/${info.getAccountID()}".toString())
        }

        return Response.ok(players).build()
    }

    @GET
    @Path('view/{playerTag}/friends')
    @Produces(MediaType.TEXT_HTML)
    public PlayerFriendsView getPlayerFriends(@PathParam("playerTag") String playerTag) {
        def player = warGamingService.searchPlayer(playerTag, "exact")
        long accountID = player.get(0).get("account_id")

        def friends = warGamingService.getPlayerFriends(accountID, "85f5e4046c243f613a6013a514108ba5068d3120")

        def playerInfo = [:]

        int count = 0
        int maxSize = 100

        while (friends.size() > count * maxSize) {
            int start = count * maxSize
            int end = (count + 1) * maxSize - 1
            end = end > (friends.size() - 1) ? friends.size() - 1 : end;
            def temp = friends[start..end]
            warGamingService.getPlayerInfo(temp).each {Long id, PlayerInfo info ->
                info.setAccountID(id)
                info.setData("http://www.noobmeter.com/player/na/${info.getNickname()}/${info.getAccountID()}".toString())
                playerInfo.put(info.getNickname(), info)
            }
            count = count + 1
        }

        return new PlayerFriendsView(playerInfo.values())
    }

    @GET
    @Path('/search/{playerTagSearch}')
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchPlayerTag(
            @PathParam("region") String region, @PathParam("playerTagSearch") String playerTagSearch) {
        def player = warGamingService.searchPlayer(playerTagSearch, "exact")

        if (player.size() == 0) {
            return Response.noContent().build()
        }

        long account_id = player.get(0).get("account_id")

        return Response.ok(account_id, MediaType.APPLICATION_JSON_TYPE).build()
    }
}

package com.absolutenoob.sherlock.service

/**
 * Created by quincy on 15-05-21.
 */
class NoobMeterService {
    //region/playertage/playerid
    public static final String STAT_URL = "http://www.noobmeter.com/player/"

    public String createURL(String region, String playerTag, long playerID) {
        return "${STAT_URL}/${region}/${playerTag}/${playerID}"
    }
}

package com.absolutenoob.sherlock.service

import com.absolutenoob.sherlock.configuration.WarGamingConfiguration
import com.absolutenoob.sherlock.core.wargaming.ClanInfo
import com.absolutenoob.sherlock.core.wargaming.ClanSummary
import com.absolutenoob.sherlock.core.wargaming.PlayerInfo
import com.absolutenoob.sherlock.core.wargaming.TankSummary
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.JsonNode

import javax.ws.rs.client.Client
import javax.ws.rs.client.Entity
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response
import java.lang.reflect.Field

/**
 * Created by quincy on 15-04-18.
 */
class WarGamingService {
    Client client
    WarGamingConfiguration warGamingConfiguration
    JsonMarshalService jsonMarshalService

    public WarGamingService(Client client, WarGamingConfiguration warGamingConfiguration) {
        this.client = client
        this.warGamingConfiguration = warGamingConfiguration
        this.jsonMarshalService = new JsonMarshalService()
    }

    public List<ClanSummary> searchClan(String clanSearchTerm) {
        def target = client.target(warGamingConfiguration.wotAPI).path("/clan/list/")

        ["application_id": this.warGamingConfiguration.getApplicationID(),
         "language": "en",
         "fields": "members_count,name,created_at,clan_id,motto,abbreviation",
         "search": clanSearchTerm].each { key, value ->
            target = target.queryParam(key, value)
        }

        def response = target.request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(null, MediaType.APPLICATION_FORM_URLENCODED_TYPE), Response.class)

        def entity = response.readEntity(String.class)

        def statusList = jsonMarshalService.unmarshalStringValue(entity, "status")

        if (statusList.contains("ok")) {
            def result = jsonMarshalService.unmarshalResponseToCollection(entity,
                    "data", List.class, ClanSummary.class)

            return result
        }
        return Collections.emptyList()
    }

    public Map<Long, ClanInfo> getClanInfo(long clanID) {
        def target = client.target(warGamingConfiguration.wotAPI).path("/clan/info/")

        ["application_id": this.warGamingConfiguration.getApplicationID(),
         "language": "en",
         "fields": "members_count,name,created_at,updated_at,abbreviation,emblems,clan_id,members",
         "clan_id": clanID].each { key, value ->
            target = target.queryParam(key, value)
        }

        def response = target.request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(null, MediaType.APPLICATION_FORM_URLENCODED_TYPE), Response.class)

        def entity = response.readEntity(String.class)

        def statusList = jsonMarshalService.unmarshalStringValue(entity, "status")

        if (statusList.contains("ok")) {
            def result = jsonMarshalService.unmarshalResponseToMap(entity,
                    "data", Long.class, ClanInfo.class)
            return result
        }

        return Collections.emptyMap()
    }

    public Map<Long, TankSummary> getTankSummaryByTier(int tier) {
        def target = this.client.target(warGamingConfiguration.getWotAPI()).path("/encyclopedia/vehicles/")

        ["application_id": warGamingConfiguration.getApplicationID(),
         "language": "en",
         "tier"  : tier,
         "fields": "tank_id,short_name,nation,type,tier,name,tag"].each { key, value ->
            target = target.queryParam(key, value)
        }

        def response = target.request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(null, MediaType.APPLICATION_FORM_URLENCODED_TYPE), Response.class)

        def entity = response.readEntity(String.class)

        def statusList = jsonMarshalService.unmarshalStringValue(entity, "status")

        if (statusList.contains("ok")) {
            def result = jsonMarshalService.unmarshalResponseToMap(entity,
                    "data", Long.class, TankSummary.class)
            return result
        }

        return Collections.emptyMap()
    }

    public List<Long> getPlayerFriends(long playerID, String token) {
        def target = client.target(warGamingConfiguration.wotAPI).path("/account/info/")

        ["application_id": this.warGamingConfiguration.getApplicationID(),
         "language": "en",
         "fields": "private.friends",
         "account_id": playerID,
         "access_token": token].each { key, value ->
            target = target.queryParam(key, value)
        }

        def response = target.request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(null, MediaType.APPLICATION_FORM_URLENCODED_TYPE), Response.class)

        def entity = response.readEntity(String.class)

        def statusList = jsonMarshalService.unmarshalStringValue(entity, "status")

        if (statusList.contains("ok")) {
            def result = jsonMarshalService.unmarshalValue(entity, "data", { JsonNode node ->
                return node.findValue("friends").asList()
            })
            return result
        }

        return Collections.emptyList()
    }

    public Map<Long, List> getPlayerVehicles(List<Long> playerIDs, List<Long> tankIDs) {
        def target = client.target(warGamingConfiguration.wotAPI).path("/account/tanks/")

        ["application_id": this.warGamingConfiguration.getApplicationID(),
         "language": "en",
         "fields": "tank_id",
         "account_id": jsonMarshalService.convertListToString(playerIDs),
         "tank_id": jsonMarshalService.convertListToString(tankIDs)].each { key, value ->
            target = target.queryParam(key, value)
        }

        def response = target.request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(null, MediaType.APPLICATION_FORM_URLENCODED_TYPE), Response.class)

        def entity = response.readEntity(String.class)

        def statusList = jsonMarshalService.unmarshalStringValue(entity, "status")

        if (statusList.contains("ok")) {

            def result = jsonMarshalService.unmarshalResponseToMap(entity,
                    "data", Long.class, List.class)
            return result
        }

        return Collections.emptyMap()
    }

    public List<Map> searchPlayer(String playerSearchTag, String searchType) {
        def target = client.target(warGamingConfiguration.wotAPI).path("/account/list/")

        ["application_id": this.warGamingConfiguration.getApplicationID(),
         "language": "en",
         "type": searchType,
         "search": playerSearchTag].each { key, value ->
            target = target.queryParam(key, value)
        }

        def response = target.request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(null, MediaType.APPLICATION_FORM_URLENCODED_TYPE), Response.class)

        def entity = response.readEntity(String.class)

        def statusList = jsonMarshalService.unmarshalStringValue(entity, "status")

        if (statusList.contains("ok")) {
            def result = jsonMarshalService.unmarshalResponseToCollection(entity,
                    "data", List.class, Map.class)

            return result
        }

        return Collections.emptyList()
    }

    public Map<String, Long> getPlayerInfo(List<Long> playerID) {
        def target = client.target(warGamingConfiguration.wotAPI).path("/account/info/")

        ["application_id": this.warGamingConfiguration.getApplicationID(),
         "language": "en",
         "fields": "nickname,created_at,last_battle_time,logout_at",
         "account_id": jsonMarshalService.convertListToString(playerID)].each { key, value ->
            target = target.queryParam(key, value)
        }

        def response = target.request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(null, MediaType.APPLICATION_FORM_URLENCODED_TYPE), Response.class)

        def entity = response.readEntity(String.class)

        def statusList = jsonMarshalService.unmarshalStringValue(entity, "status")

        if (statusList.contains("ok")) {
            def result = jsonMarshalService.unmarshalResponseToMap(entity, "data", Long.class, PlayerInfo.class)
            return result
        }

        return Collections.emptyMap()
    }

    public String login(String redirectURI) {
        def target = client.target(warGamingConfiguration.wotAPI).path("/auth/login/")

        ["application_id": warGamingConfiguration.getApplicationID(),
         "redirect_uri": redirectURI,
         "nofollow": 1].each { key, value ->
            target = target.queryParam(key, value)
        }

        def response = target.request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(null, MediaType.APPLICATION_FORM_URLENCODED_TYPE), Response.class)

        def entity = response.readEntity(String.class)

        def statusList = jsonMarshalService.unmarshalStringValue(entity, "status")

        if (statusList.contains("ok")) {
            def result = jsonMarshalService.unmarshalResponseToMap(entity, "data", String.class, String.class)
            return result.get("location")
        }

        return ""
    }
}

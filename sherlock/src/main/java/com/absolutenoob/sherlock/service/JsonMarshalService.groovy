package com.absolutenoob.sherlock.service

import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper

/**
 * Created by quincy on 15-04-17.
 */
class JsonMarshalService {
    ObjectMapper objectMapper
    JsonFactory jsonFactory

    public JsonMarshalService() {
        jsonFactory = new JsonFactory()

        this.objectMapper = new ObjectMapper()
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
    }

    public <C, E> C<E> unmarshalResponseToCollection(String input, String keyWord, Class<C> collectionClass, Class<E> elementClass) {
        def node = objectMapper.readValue(input, JsonNode.class)
        def valueNode = node.findValue(keyWord)
        def result = objectMapper.readValue(valueNode.toString(), objectMapper.getTypeFactory().constructCollectionType(collectionClass, elementClass))
        return result
    }

    public <K, V> Map<K, V> unmarshalResponseToMap(String input, String keyWord, Class<K> keyClass, Class<V> elementClass) {
        def node = objectMapper.readValue(input, JsonNode.class)
        def valueNode = node.findValue(keyWord)
        def result = objectMapper.readValue(valueNode.toString(), objectMapper.getTypeFactory().constructMapType(HashMap.class, keyClass, elementClass))
        return result
    }

    public List<String> unmarshalStringValue(String input, String keyWord) {
        def node = objectMapper.readValue(input, JsonNode.class)
        return node.findValuesAsText(keyWord)
    }

    public <T> T unmarshalValue(String input, String keyWord, Closure<T> closure) {
        def node = objectMapper.readValue(input, JsonNode.class)
        def valueNode = node.findValue(keyWord)
        return closure.call(valueNode)
    }

    public String marshal(Object input) {
        this.objectMapper.writeValueAsString(input)
    }

    public String convertListToString(List input) {
        def stringBuilder = new StringBuilder()
        input.each { it ->
            stringBuilder.append("${it.toString()},")
        }
        def result = stringBuilder.toString()
        return result.substring(0, result.length() - 1)
    }
}

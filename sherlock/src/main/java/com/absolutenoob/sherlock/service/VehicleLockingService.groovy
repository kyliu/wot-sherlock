package com.absolutenoob.sherlock.service

/**
 * Created by quincy on 15-04-15.
 */
class VehicleLockingService {
    private static final int MINUTE_IN_HOUR = 60
    Map<String, int[]> normalRules = new HashMap<>()

    Map<String, Map<String, int[]>> attackAndCaptureRule = new HashMap<>()

    void populateNormalRule() {
        def lightTankRule = ["0", "1", "2", "4", "16", "16", "16", "48", "0", "0"].collect { hourStringToMinutes(it) }
        def mediumTankRule = ["0", "1", "4", "16", "25", "30", "48", "72", "96", "120"].collect {
            hourStringToMinutes(it)
        }
        def heavyTankRule = ["0", "0", "0", "24", "30", "48", "72", "96", "120", "168"].collect {
            hourStringToMinutes(it)
        }
        def tankDestroyerRule = ["0", "1", "4", "16", "25", "30", "48", "72", "96", "96"].collect {
            hourStringToMinutes(it)
        }
        def spgRule = ["0", "4", "8", "18", "27", "36", "50", "64", "72", "72"].collect { hourStringToMinutes(it) }

        normalRules.put("lightTank", lightTankRule)
        normalRules.put("mediumTank", mediumTankRule)
        normalRules.put("heavyTank", heavyTankRule)
        normalRules.put("AT-SPG", tankDestroyerRule)
        normalRules.put("SPG", spgRule)
    }

    int minutize(String hourString) {
        return minutize(Integer.valueOf(hourString))
    }

    int minutize(int hour) {
        return hour * MINUTE_IN_HOUR
    }

    int hourStringToMinutes(String hourString) {
        if (hourString == null) {
            return 0
        }

        def hoursAndMinutes = hourString.split(":")

        if (hoursAndMinutes.length == 1) {
            return minutize(hoursAndMinutes[0])
        } else if (hoursAndMinutes.length == 2) {
            return minutize(hoursAndMinutes[0]) + Integer.valueOf(hoursAndMinutes[1])
        } else {
            throw IllegalArgumentException("Invalid input string ${hourString}")
        }
    }

    void populateAttackAndCaptureRule() {
        def lightTankNormalRule = ["0", "0:30", "1", "2", "8", "8", "8", "24", "0", "0"].collect {
            hourStringToMinutes(it)
        }
        def lightTankGoldRule = ["0", "0:06", "0:12", "0:24", "1:36", "1:36", "1:36", "4:48", "0", "0"].collect {
            hourStringToMinutes(it)
        }
        def lightTankLandingRule = ["0", "0:12", "0:24", "0:48", "3:12", "3:12", "3:12", "9:36", "0", "0"].collect {
            hourStringToMinutes(it)
        }

        def lightTankRule = new HashMap<String, int[]>()
        lightTankRule.put("normal", lightTankNormalRule)
        lightTankRule.put("gold", lightTankGoldRule)
        lightTankRule.put("start", lightTankLandingRule)

        def mediumTankNormalRule = ["0", "0:30", "2", "8", "12:30", "15", "24", "36", "48", "60"].collect {
            hourStringToMinutes(it)
        }
        def mediumTankGoldRule = ["0", "0:06", "0:24", "1:36", "2:30", "3", "4:48", "7:12", "9:36", "12"].collect {
            hourStringToMinutes(it)
        }
        def mediumTankLandingRule = ["0", "0:12", "0:48", "3:12", "5", "6", "9:36", "14:24", "19:12", "24"].collect {
            hourStringToMinutes(it)
        }

        def mediumTankRule = new HashMap<String, int[]>()
        mediumTankRule.put("normal", mediumTankNormalRule)
        mediumTankRule.put("gold", mediumTankGoldRule)
        mediumTankRule.put("start", mediumTankLandingRule)

        def heavyTankNormalRule = ["0", "0", "0", "12", "15", "24", "36", "48", "60", "84"].collect {
            hourStringToMinutes(it)
        }
        def heavyTankGoldRule = ["0", "0", "0", "2:24", "3", "4:48", "7:12", "9:36", "12", "16:48"].collect {
            hourStringToMinutes(it)
        }
        def heavyTankLandingRule = ["0", "0", "0", "4:48", "6", "9:36", "14:24", "19:12", "24", "33:36"].collect {
            hourStringToMinutes(it)
        }

        def heavyTankRule = new HashMap<String, int[]>()
        heavyTankRule.put("normal", heavyTankNormalRule)
        heavyTankRule.put("gold", heavyTankGoldRule)
        heavyTankRule.put("start", heavyTankLandingRule)

        def tankDestroyerNormalRule = ["0", "0:30", "2", "8", "12:30", "15", "24", "36", "48", "48"].collect {
            hourStringToMinutes(it)
        }
        def tankDestroyerGoldRule = ["0", "0:06", "0:24", "1:36", "2:30", "3", "4:48", "7:12", "9:36", "9:36"].collect {
            hourStringToMinutes(it)
        }
        def tankDestroyerLandingRule = ["0", "0:12", "0:48", "3:12", "5", "6", "9:36", "14:24", "19:12", "19:12"].collect {
            hourStringToMinutes(it)
        }

        def tankDestroyerRule = new HashMap<String, int[]>()
        tankDestroyerRule.put("normal", tankDestroyerNormalRule)
        tankDestroyerRule.put("gold", tankDestroyerGoldRule)
        tankDestroyerRule.put("start", tankDestroyerLandingRule)

        def spgNormalRule = ["0", "2", "4", "9", "13:30", "18", "25", "32", "36", "36"].collect {
            hourStringToMinutes(it)
        }
        def spgGoldRule = ["0", "0:24", "0:48", "1:48", "2:42", "3:36", "5", "6:24", "7:12", "7:12"].collect {
            hourStringToMinutes(it)
        }
        def spgLandingRule = ["0", "0:48", "1:36", "3:36", "5:24", "7:12", "10", "12:48", "14:24", "14:24"].collect {
            hourStringToMinutes(it)
        }

        def spgRule = new HashMap<String, int[]>()
        spgRule.put("normal", spgNormalRule)
        spgRule.put("gold", spgGoldRule)
        spgRule.put("start", spgLandingRule)

        this.attackAndCaptureRule.put("lightTank", lightTankRule)
        this.attackAndCaptureRule.put("mediumTank", mediumTankRule)
        this.attackAndCaptureRule.put("heavyTank", heavyTankRule)
        this.attackAndCaptureRule.put("AT-SPG", tankDestroyerRule)
        this.attackAndCaptureRule.put("SPG", spgRule)
    }

    Map<String, int[]> getRuleForMapType(String mapType) {
        def result = new HashMap<String, int[]>()
        def lightTankRule = this.attackAndCaptureRule.get("lightTank").get(mapType)
        result.put("lightTank", lightTankRule)
        def mediumTankRule = this.attackAndCaptureRule.get("mediumTank").get(mapType)
        result.put("mediumTank", mediumTankRule)
        def heavyTankRule = this.attackAndCaptureRule.get("heavyTank").get(mapType)
        result.put("heavyTank", heavyTankRule)
        def tankDestroyerRule = this.attackAndCaptureRule.get("AT-SPG").get(mapType)
        result.put("AT-SPG", tankDestroyerRule)
        def spgRule = this.attackAndCaptureRule.get("SPG").get(mapType)
        result.put("SPG", spgRule)
        return result
    }
}


<#-- @ftlvariable name="" type="com.absolutenoob.sherlock.views.PlayerFriendsView" -->
<html>
<head>
    <title>Player</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/theme.dropbox.css" rel="stylesheet">
    <!-- load jQuery and tablesorter scripts -->
    <script type="text/javascript" src="/assets/js/jquery-2.1.3.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.22.1/js/jquery.tablesorter.min.js"></script>
</head>
<body>
<div id="modal-container"></div>
<div id="container" class="container">
    <table id="friends_table" class="tablesorter">
        <thead>
        <tr>
            <th>Nickname</th>
            <th>Created</th>
            <th>Log Out</th>
            <th>Last Battle</th>
            <th>Noob Meter Link</th>
        </tr>
        </thead>
        <tbody>
        <#list playerInfos as playerInfo>
        <tr>
            <td>${playerInfo.getNickname()}</td>
            <td>${playerInfo.getCreated()}</td>
            <td>${playerInfo.getLogOut()}</td>
            <td>${playerInfo.getLastBattle()}</td>
            <td><a href="${playerInfo.getData()}" target="_blank">${playerInfo.getNickname()}</a></td>
        </tr>
        </#list>
        </tbody>
    </table>
</div>
<script>
    $(function(){
        $("#friends_table").tablesorter({
            theme : 'dropbox'
        });
    });
</script>
</body>
</html>


#!/usr/bin/env bash

wget --no-check-certificate https://raw.githubusercontent.com/quincyl/WoT-Dossier-Cache-to-JSON/master/maps.json -P WoT-Replay-To-JSON/
wget --no-check-certificate https://raw.githubusercontent.com/quincyl/WoT-Dossier-Cache-to-JSON/master/tanks.json -P WoT-Replay-To-JSON/

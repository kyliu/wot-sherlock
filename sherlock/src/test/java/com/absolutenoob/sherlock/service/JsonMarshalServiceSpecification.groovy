package com.absolutenoob

import com.absolutenoob.sherlock.core.wargaming.ClanInfo
import com.absolutenoob.sherlock.core.wargaming.ClanSummary
import com.absolutenoob.sherlock.service.JsonMarshalService
import spock.lang.Specification

/**
 * Created by quincy on 15-04-17.
 */
class JsonMarshalServiceSpecification extends Specification {
    // fields
    static String jsonString
    static String jsonClanInfo
    static String jsonResultExpected
    static String jsonClanInfoNoMembers
    JsonMarshalService jsonMarshalService

    // fixture methods
    def setup() { // run before every feature method
        jsonMarshalService = new JsonMarshalService()
    }

    def cleanup() { // run after every feature method
        jsonMarshalService = null
    }

    def setupSpec() { // run before the first feature method
        jsonString = '''{
    "status": "ok",
    "count": 3,
    "meta": {
        "count": 3,
        "total": 3
    },
    "data": [
        {
            "members_count": 4,
            "name": "I'm on a Tank",
            "created_at": 1302142893,
            "abbreviation": "IONAT",
            "clan_id": 1000000473,
            "motto": "We're in our tanks, _________-_______s"
        },
        {
            "members_count": 97,
            "name": "ION",
            "created_at": 1427766088,
            "abbreviation": "ION",
            "clan_id": 1000020531,
            "motto": "\\"When you form Voltron, you need every piece\\""
        },
        {
            "members_count": 12,
            "name": "Ioncross Community",
            "created_at": 1355452471,
            "abbreviation": "-ION-",
            "clan_id": 1000007040,
            "motto": "Tractor My Stuff!"
        }
    ]
}'''
        jsonResultExpected = '''[
        {
            "members_count": 4,
            "name": "I'm on a Tank",
            "created_at": 1302142893,
            "abbreviation": "IONAT",
            "clan_id": 1000000473,
            "motto": "We're in our tanks, _________-_______s"
        },
        {
            "members_count": 97,
            "name": "ION",
            "created_at": 1427766088,
            "abbreviation": "ION",
            "clan_id": 1000020531,
            "motto": "\\"When you form Voltron, you need every piece\\""
        },
        {
            "members_count": 12,
            "name": "Ioncross Community",
            "created_at": 1355452471,
            "abbreviation": "-ION-",
            "clan_id": 1000007040,
            "motto": "Tractor My Stuff!"
        }
    ]
'''
        jsonClanInfo = '''
{
    "status": "ok",
    "count": 1,
    "meta": {
        "count": 1
    },
    "data": {
        "1000020531": {
            "members_count": 98,
            "name": "ION",
            "created_at": 1427766088,
            "updated_at": 1429329620,
            "abbreviation": "ION",
            "emblems": {
                "large": "http://clans.worldoftanks.com/media/clans/emblems/cl_531/1000020531/emblem_64x64.png",
                "small": "http://clans.worldoftanks.com/media/clans/emblems/cl_531/1000020531/emblem_24x24.png",
                "medium": "http://clans.worldoftanks.com/media/clans/emblems/cl_531/1000020531/emblem_32x32.png",
                "bw_tank": "http://clans.worldoftanks.com/media/clans/emblems/cl_531/1000020531/emblem_64x64_tank.png"
            },
            "clan_id": 1000020531,
            "members": {
                "1000020936": {
                    "created_at": 1427948160,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1000020936,
                    "account_name": "Kassimila"
                },
                "1000024973": {
                    "created_at": 1427950814,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1000024973,
                    "account_name": "Blight111"
                },
                "1000045749": {
                    "created_at": 1428021429,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1000045749,
                    "account_name": "Rakshan"
                },
                "1000052426": {
                    "created_at": 1428031772,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1000052426,
                    "account_name": "vespid"
                },
                "1000057069": {
                    "created_at": 1427946920,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1000057069,
                    "account_name": "WarWolverineWarrior"
                },
                "1000228876": {
                    "created_at": 1427986750,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1000228876,
                    "account_name": "thepager"
                },
                "1000258280": {
                    "created_at": 1428635959,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1000258280,
                    "account_name": "Jracule"
                },
                "1000293518": {
                    "created_at": 1427943285,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1000293518,
                    "account_name": "The_Juggernaut"
                },
                "1000314493": {
                    "created_at": 1428023069,
                    "role": "recruiter",
                    "role_i18n": "Recruitment Officer",
                    "account_id": 1000314493,
                    "account_name": "Dozer666"
                },
                "1000331631": {
                    "created_at": 1427941468,
                    "role": "vice_leader",
                    "role_i18n": "Executive Officer",
                    "account_id": 1000331631,
                    "account_name": "Hawkks"
                },
                "1000351164": {
                    "created_at": 1427945353,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1000351164,
                    "account_name": "osirislod"
                },
                "1000370269": {
                    "created_at": 1427986806,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1000370269,
                    "account_name": "A_Dead_Duck"
                },
                "1000676564": {
                    "created_at": 1427948856,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1000676564,
                    "account_name": "squatchi"
                },
                "1000754736": {
                    "created_at": 1428110971,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1000754736,
                    "account_name": "fakeplasticman"
                },
                "1000776547": {
                    "created_at": 1428379071,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1000776547,
                    "account_name": "owlgator"
                },
                "1000784065": {
                    "created_at": 1427942707,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1000784065,
                    "account_name": "Farout1974_"
                },
                "1000811813": {
                    "created_at": 1428861292,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1000811813,
                    "account_name": "James_F"
                },
                "1000872480": {
                    "created_at": 1429329226,
                    "role": "recruit",
                    "role_i18n": "Recruit",
                    "account_id": 1000872480,
                    "account_name": "RKubica32"
                },
                "1001002486": {
                    "created_at": 1427946496,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1001002486,
                    "account_name": "Hairman4020"
                },
                "1001005630": {
                    "created_at": 1427940667,
                    "role": "vice_leader",
                    "role_i18n": "Executive Officer",
                    "account_id": 1001005630,
                    "account_name": "HondoDuke"
                },
                "1001062173": {
                    "created_at": 1427941265,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1001062173,
                    "account_name": "Conman777"
                },
                "1001075145": {
                    "created_at": 1427941360,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1001075145,
                    "account_name": "ASA502"
                },
                "1001097258": {
                    "created_at": 1428549468,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1001097258,
                    "account_name": "danielalexander81"
                },
                "1001139427": {
                    "created_at": 1428102286,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1001139427,
                    "account_name": "Bitter"
                },
                "1001248955": {
                    "created_at": 1427952637,
                    "role": "vice_leader",
                    "role_i18n": "Executive Officer",
                    "account_id": 1001248955,
                    "account_name": "sofakin"
                },
                "1001273824": {
                    "created_at": 1428021426,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1001273824,
                    "account_name": "Kohler7"
                },
                "1001543103": {
                    "created_at": 1427941110,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1001543103,
                    "account_name": "Eraden"
                },
                "1001569038": {
                    "created_at": 1427991581,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1001569038,
                    "account_name": "KeraZala"
                },
                "1001614901": {
                    "created_at": 1428366942,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1001614901,
                    "account_name": "Atrophy"
                },
                "1001636370": {
                    "created_at": 1427948530,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1001636370,
                    "account_name": "Quadman"
                },
                "1001693615": {
                    "created_at": 1427946723,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1001693615,
                    "account_name": "T2KKordent"
                },
                "1001699370": {
                    "created_at": 1427939411,
                    "role": "vice_leader",
                    "role_i18n": "Executive Officer",
                    "account_id": 1001699370,
                    "account_name": "InEx"
                },
                "1001749132": {
                    "created_at": 1428197684,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1001749132,
                    "account_name": "Mohoao"
                },
                "1001770918": {
                    "created_at": 1428018761,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1001770918,
                    "account_name": "kilroy077"
                },
                "1001888308": {
                    "created_at": 1428016272,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1001888308,
                    "account_name": "Dregin"
                },
                "1001902958": {
                    "created_at": 1428107750,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1001902958,
                    "account_name": "MRDOtn"
                },
                "1001954905": {
                    "created_at": 1428022624,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1001954905,
                    "account_name": "Mudslanger"
                },
                "1001976565": {
                    "created_at": 1427943511,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1001976565,
                    "account_name": "warrllord51"
                },
                "1001993165": {
                    "created_at": 1428009584,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1001993165,
                    "account_name": "Zeuxis"
                },
                "1002074298": {
                    "created_at": 1428012562,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1002074298,
                    "account_name": "dav78215"
                },
                "1002238298": {
                    "created_at": 1427940219,
                    "role": "vice_leader",
                    "role_i18n": "Executive Officer",
                    "account_id": 1002238298,
                    "account_name": "PriorityAces"
                },
                "1002374591": {
                    "created_at": 1427943774,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1002374591,
                    "account_name": "Ebrola"
                },
                "1002487312": {
                    "created_at": 1428165609,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1002487312,
                    "account_name": "beastyboi"
                },
                "1002569013": {
                    "created_at": 1427942848,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1002569013,
                    "account_name": "Z3zen"
                },
                "1002613315": {
                    "created_at": 1428025948,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1002613315,
                    "account_name": "BigSexyLumberjack"
                },
                "1002659242": {
                    "created_at": 1428273801,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1002659242,
                    "account_name": "kingfox65"
                },
                "1002663931": {
                    "created_at": 1428404400,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1002663931,
                    "account_name": "VonHartig"
                },
                "1002666445": {
                    "created_at": 1427946245,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1002666445,
                    "account_name": "EATatJOES"
                },
                "1002788153": {
                    "created_at": 1428025820,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1002788153,
                    "account_name": "electronicnightmare"
                },
                "1002907772": {
                    "created_at": 1428366087,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1002907772,
                    "account_name": "Decayy"
                },
                "1002925529": {
                    "created_at": 1427941217,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1002925529,
                    "account_name": "BlessD1"
                },
                "1002985647": {
                    "created_at": 1428102141,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1002985647,
                    "account_name": "Quicksaber"
                },
                "1003059901": {
                    "created_at": 1428195577,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1003059901,
                    "account_name": "cartouche"
                },
                "1003176068": {
                    "created_at": 1428035350,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1003176068,
                    "account_name": "EDIDRI6"
                },
                "1003203884": {
                    "created_at": 1428027469,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1003203884,
                    "account_name": "LordKefka"
                },
                "1003334698": {
                    "created_at": 1428012570,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1003334698,
                    "account_name": "Proculus"
                },
                "1003352886": {
                    "created_at": 1428978912,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1003352886,
                    "account_name": "Toxic_Turtles690"
                },
                "1003383341": {
                    "created_at": 1428017407,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1003383341,
                    "account_name": "ColSanderz"
                },
                "1003411137": {
                    "created_at": 1428024450,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1003411137,
                    "account_name": "wwhyme"
                },
                "1003414240": {
                    "created_at": 1428022635,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1003414240,
                    "account_name": "gord0"
                },
                "1003589224": {
                    "created_at": 1427952008,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1003589224,
                    "account_name": "arthurH"
                },
                "1003608536": {
                    "created_at": 1427941249,
                    "role": "vice_leader",
                    "role_i18n": "Executive Officer",
                    "account_id": 1003608536,
                    "account_name": "xRepent"
                },
                "1003665687": {
                    "created_at": 1427951692,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1003665687,
                    "account_name": "_Snooze"
                },
                "1003670363": {
                    "created_at": 1427944284,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1003670363,
                    "account_name": "soap750"
                },
                "1003714447": {
                    "created_at": 1427940776,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1003714447,
                    "account_name": "PsychoSly"
                },
                "1003714521": {
                    "created_at": 1427939267,
                    "role": "recruiter",
                    "role_i18n": "Recruitment Officer",
                    "account_id": 1003714521,
                    "account_name": "A_Horse_With_No_Name"
                },
                "1003743629": {
                    "created_at": 1427942654,
                    "role": "vice_leader",
                    "role_i18n": "Executive Officer",
                    "account_id": 1003743629,
                    "account_name": "Bruce8585"
                },
                "1003768784": {
                    "created_at": 1428017755,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1003768784,
                    "account_name": "AHarmlessDolphin"
                },
                "1003813275": {
                    "created_at": 1427946321,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1003813275,
                    "account_name": "Prometh3us"
                },
                "1003836936": {
                    "created_at": 1428017286,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1003836936,
                    "account_name": "TheSlayer1"
                },
                "1003842915": {
                    "created_at": 1428130240,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1003842915,
                    "account_name": "DevilNoTears341"
                },
                "1003910266": {
                    "created_at": 1427942223,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1003910266,
                    "account_name": "markdaspot"
                },
                "1004184419": {
                    "created_at": 1428020099,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1004184419,
                    "account_name": "SlagSr"
                },
                "1004408301": {
                    "created_at": 1427988108,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1004408301,
                    "account_name": "Jendragon"
                },
                "1004427874": {
                    "created_at": 1428282791,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1004427874,
                    "account_name": "Captain_Cessna"
                },
                "1004473429": {
                    "created_at": 1428123302,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1004473429,
                    "account_name": "CakeOrD3ath"
                },
                "1004508416": {
                    "created_at": 1427939913,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1004508416,
                    "account_name": "Kevlar2013"
                },
                "1004587146": {
                    "created_at": 1428030649,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1004587146,
                    "account_name": "Spamium"
                },
                "1004645240": {
                    "created_at": 1428107740,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1004645240,
                    "account_name": "Superbuzzard"
                },
                "1004649398": {
                    "created_at": 1428031304,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1004649398,
                    "account_name": "XReplacantX"
                },
                "1004745067": {
                    "created_at": 1427991270,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1004745067,
                    "account_name": "Angry_Kululu"
                },
                "1004810129": {
                    "created_at": 1427944258,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1004810129,
                    "account_name": "11Trigger11"
                },
                "1004867155": {
                    "created_at": 1428025823,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1004867155,
                    "account_name": "Jinshan"
                },
                "1005045515": {
                    "created_at": 1427940242,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1005045515,
                    "account_name": "Armydude140"
                },
                "1005139215": {
                    "created_at": 1427942366,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1005139215,
                    "account_name": "axiskiller343"
                },
                "1005361830": {
                    "created_at": 1427939771,
                    "role": "vice_leader",
                    "role_i18n": "Executive Officer",
                    "account_id": 1005361830,
                    "account_name": "Phatchanz"
                },
                "1005447973": {
                    "created_at": 1428025816,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1005447973,
                    "account_name": "Lord_Met"
                },
                "1005513938": {
                    "created_at": 1427942843,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1005513938,
                    "account_name": "FlankaTanka"
                },
                "1005533055": {
                    "created_at": 1427854134,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1005533055,
                    "account_name": "e4601"
                },
                "1005808163": {
                    "created_at": 1428016344,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1005808163,
                    "account_name": "pacman2426"
                },
                "1005812885": {
                    "created_at": 1427941271,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1005812885,
                    "account_name": "Stevoni"
                },
                "1005813725": {
                    "created_at": 1428289849,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1005813725,
                    "account_name": "TickleMeTmo"
                },
                "1006239770": {
                    "created_at": 1427943133,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1006239770,
                    "account_name": "aibo"
                },
                "1006295065": {
                    "created_at": 1428023594,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1006295065,
                    "account_name": "KittWalker911"
                },
                "1006332842": {
                    "created_at": 1427944786,
                    "role": "vice_leader",
                    "role_i18n": "Executive Officer",
                    "account_id": 1006332842,
                    "account_name": "Drunkenwiseman"
                },
                "1008143841": {
                    "created_at": 1427952108,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1008143841,
                    "account_name": "Youkai646"
                },
                "1011603793": {
                    "created_at": 1428033193,
                    "role": "private",
                    "role_i18n": "Private",
                    "account_id": 1011603793,
                    "account_name": "lubert"
                },
                "1013486432": {
                    "created_at": 1427766088,
                    "role": "leader",
                    "role_i18n": "Commander",
                    "account_id": 1013486432,
                    "account_name": "ugh_ack"
                }
            }
        }
    }
}'''

        jsonClanInfoNoMembers = '''
{
    "status": "ok",
    "count": 1,
    "meta": {
        "count": 1
    },
    "data": {
        "1000020531": {
            "members_count": 98,
            "name": "ION",
            "created_at": 1427766088,
            "updated_at": 1429329620,
            "abbreviation": "ION",
            "clan_id": 1000020531
        }
    }
}'''
    }

    def cleanupSpec() { // run after the last feature method

    }

    // feature methods
    def "create JsonParser at desired spot"() {
        when:
        def key = "count"
        def jp = jsonMarshalService.createJsonParser(jsonString, key)

        then:
        jp.nextToken()

        expect:
        assert jp.getCurrentName() == key

        jp.nextToken()
        assert jp.getCurrentName() != key
    }

    def "create JsonParser with null key"() {
        when:
        def jp = jsonMarshalService.createJsonParser(jsonString, null)

        then:
        thrown(IllegalArgumentException)
    }

    def "create JsonParser with invalid key"() {
        when:
        def jp = jsonMarshalService.createJsonParser(jsonString, "dsfdsswe")

        then:
        assert jp.getCurrentName() == null
    }

    def "parse json string"() {
        when:
        Map<Long, ClanInfo> result = jsonMarshalService.unmarshalResponseToCollection(jsonString, "data", List.class, ClanSummary.class)

        then:
        println "hello"
    }

    def "parse json clan info"() {
        when:
        Map<Long, ClanInfo> result = jsonMarshalService.unmarshalResponseToMap(jsonClanInfo, "data", Long.class, ClanInfo.class)

        then:
        result.each { key, value ->
            println value
            println value.getEmblems()
            println value.getMembers()
        }

    }

    // helper methods
}

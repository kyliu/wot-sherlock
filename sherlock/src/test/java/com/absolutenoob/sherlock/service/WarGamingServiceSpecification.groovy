package com.absolutenoob

import com.absolutenoob.sherlock.configuration.WarGamingConfiguration
import com.absolutenoob.sherlock.service.WarGamingService
import spock.lang.Specification

import javax.ws.rs.client.Client
import javax.ws.rs.client.ClientBuilder

/**
 * Created by quincy on 15-04-18.
 */
class WarGamingServiceSpecification extends Specification {
    static WarGamingConfiguration configuration
    static Client client
    WarGamingService warGamingService

    def setupSpec() {
        configuration = new WarGamingConfiguration()
        configuration.setApplicationID("13369c660a54d3cc7f92390f39a3655c")
        configuration.setWotAPI("https://api.worldoftanks.com/wot/")

        client = ClientBuilder.newClient()
    }

    def setup() {
        warGamingService = new WarGamingService(client, configuration)
    }

    def "get all tier 10 tanks summary"() {
        when:
        def result = warGamingService.getTankSummaryByTier(10)

        then:
        assert result.size() == 37
        result.each { key, value -> assert value.getTier() == 10 }
    }

    def "get tanks from players"() {
        def accoundIds = [1000020936, 1000024973, 1000045749, 1000052426, 1000057069, 1000228876, 1000258280, 1000293518, 1000314493, 1000331631, 1000351164, 1000370269, 1000676564, 1000754736, 1000776547, 1000784065, 1000811813, 1001002486, 1001005630, 1001062173, 1001075145, 1001097258, 1001139427, 1001248955, 1001273824, 1001543103, 1001569038, 1001614901, 1001636370, 1001693615, 1001699370, 1001749132, 1001770918, 1001888308, 1001902958, 1001954905, 1001976565, 1001993165, 1002074298, 1002238298, 1002374591, 1002487312, 1002569013, 1002613315, 1002659242, 1002663931, 1002666445, 1002788153, 1002809770, 1002907772, 1002925529, 1002985647, 1003059901, 1003176068, 1003203884, 1003334698, 1003352886, 1003383341, 1003411137, 1003414240, 1003589224, 1003608536, 1003665687, 1003670363, 1003714447, 1003714521, 1003743629, 1003768784, 1003813275, 1003836936, 1003842915, 1003910266, 1004184419, 1004408301, 1004427874, 1004473429, 1004508416, 1004587146, 1004645240, 1004649398, 1004745067, 1004810129, 1004867155, 1005045515, 1005139215, 1005361830, 1005447973, 1005513938, 1005533055, 1005808163, 1005812885, 1005813725, 1006239770, 1006295065, 1006332842, 1008143841, 1011603793, 1013486432]

        def tierXTankIds = [13825, 7169, 16897, 15617, 17153, 58369, 13569, 11841, 6209, 6145, 3649, 13889, 8705, 14337, 9489, 6929, 13905, 12049, 12305, 12369, 16913, 9233, 7249, 6225, 9297, 58641, 14609, 3681, 13089, 15905, 14113, 14881, 8481, 13857, 10785, 4145, 5425]

        when:
        def result = warGamingService.getPlayerVehicles(accoundIds, tierXTankIds)

        def sum = result.collect { Long key, List value -> value.size() }.sum()

        then:
        assert result.size() != 0
        assert result.size() <= sum
    }

    def "time"() {
        when:
        def date = new Date(System.currentTimeMillis()+1430869154)
        then:
        println date.toString()
    }
}

package com.absolutenoob

import io.dropwizard.Application
import io.dropwizard.setup.Environment

/**
 * Created by quincy on 15-04-17.
 */
class WatsonApplication extends Application<WatsonConfiguration> {

    public static void main(String[] args) {
        new WatsonApplication().run(args)
    }

    @Override
    void run(WatsonConfiguration watsonConfiguration, Environment environment) throws Exception {

    }
}
